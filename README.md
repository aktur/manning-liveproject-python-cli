# Manning liveproject Build an Extensible CLI in Python
[![Build Status](https://travis-ci.com/aktur/manning-liveproject-python-cli.svg?branch=master)](https://travis-ci.com/aktur/manning-liveproject-python-cli)

## Milestone 1 - Create CLI Skeleton and CI/CD Pipeline

### Objective

- Build a skeleton of the Klickbrick CLI with a simple “hello world” command using the argparse module to establish a pattern for automated testing, building with Travis CI, and publishing the CLI to PyPI for distribution.

## Milestone 2 - Add Onboarding CLI Command

### Objective

In Milestone 2 you will add the first real command to the CLI, which will automate the onboarding experience for a new developer at KlickBrick.

This onboarding experience involves tasks like installing a common toolchain, customizing for company standards, and validating your working configuration.

## Milestone 3 - Add New Project CLI Command
### Objective

KlickBrick is expanding its digital efforts to accelerate new projects by standardizing conventions for compliance and security and for consistency across teams. Since new developers will use the CLI to support onboarding, a natural next step is to add a command to the CLI for initializing a new software project repository.