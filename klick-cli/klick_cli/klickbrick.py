import argparse
import sys


def parse_args(args):
    parser = argparse.ArgumentParser(description="Marvelous Manning CLI liveproject")
    parser.add_argument(
        "-v",
        "--version",
        action="version",
        version="0.3.0",
    )
    parser.add_argument(
        "-d",
        "--dry-run",
        action="store_true",
        help="Don't execute any action, just check for errors",
    )

    subparsers = parser.add_subparsers(title="command", dest="command")

    # hello command
    parser_a = subparsers.add_parser("hello", help="hello world example")
    parser_a.add_argument(
        "-n",
        "--name",
        type=str,
        default="World",
        help="Optional flag to be more personal",
    )

    # onboard command
    parser_b = subparsers.add_parser("onboard", help="employee onboard")

    parser_b.add_argument("--checklist", help="Generate checklist", action="store_true")

    parser_b.add_argument("--first-name", type=str, default="Empty", help="First name")

    parser_b.add_argument("--last-name", type=str, default="Empty", help="Last name")

    parser_b.add_argument(
        "--it-request", help="Make request to IT department", action="store_true"
    )

    parser_b.add_argument(
        "--dev-tools",
        nargs="?",
        const="all",
        choices=["git", "pyenv", "poetry", "all"],
        help="Install development tools",
    )

    return parser.parse_args(args)


def hello(arg):
    return "Hello " + arg


def main():
    args = parse_args(sys.argv[1:])

    if args.command == "hello":
        print(hello(args.name))
    elif args.command == "onboard":
        output = ""
        if args.dry_run:
            output = "dry-run: "
        if args.checklist:
            print(output + "checklist")
        elif args.it_request:
            print(output + "IT request")
        elif args.dev_tools:
            print(output + "Dev tools: ", args.dev_tools)
        else:
            print(output + "Full onboard")
    elif args.command:
        print("Unknown command: " + args.command)
    else:
        parse_args(["--help"])


if __name__ == "__main__":
    main()
