import unittest
import klick_cli.klickbrick as kb


class TestStringMethods(unittest.TestCase):
    def test_upper(self):
        self.assertEqual(kb.hello("Ole"), "Hello Ole")


if __name__ == "__main__":
    unittest.main()
