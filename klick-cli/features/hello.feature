Feature: hello world

  Scenario: test default
    When we call hello command
    Then we get 'Hello World' response

  Scenario: test --name
    When we call hello with -n parameter
    Then we get 'Hello parameter'

  Scenario: test unknow command
    Given we call klick with unknown command
    Then it should show invalid choice