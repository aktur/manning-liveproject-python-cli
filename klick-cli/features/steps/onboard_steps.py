from behave import *
import sys
from klick_cli.klickbrick import main
from mock import patch


@given(u"we call onboard --checklist")
def step_impl(context):
    with patch.object(sys, "argv", ["klick", "onboard", "--checklist"]):
        main()


@then(u"we get checklist")
def step_impl(context):
    output = context.stdout_capture.getvalue()
    assert output == "checklist\n"


@given(u"we call onboard --it-request --first-name Ole --last-name Christiansen")
def step_impl(context):
    with patch.object(
        sys,
        "argv",
        [
            "klick",
            "onboard",
            "--it-request",
            "--first-name",
            "Ole",
            "--last-name",
            "Christiansen",
        ],
    ):
        main()


@then(u"send email")
def step_impl(context):
    output = context.stdout_capture.getvalue()
    assert output == "IT request\n"


@given(u"we call onboard --dev-tools")
def step_impl(context):
    with patch.object(sys, "argv", ["klick", "onboard", "--dev-tools"]):
        main()


@then(u"install all tools")
def step_impl(context):
    output = context.stdout_capture.getvalue()
    assert output == "Dev tools:  all\n"


@given(u"we call onboard --dev-tools [git | pyenv | poetry]")
def step_impl(context):
    with patch.object(sys, "argv", ["klick", "onboard", "--dev-tools", "git"]):
        main()


@then(u"install specific tool")
def step_impl(context):
    output = context.stdout_capture.getvalue()
    assert output == "Dev tools:  git\n"


@given(u"we call klickbrick onboard --first-name Ole --last-name Christiansen")
def step_impl(context):
    with patch.object(
        sys,
        "argv",
        ["klick", "onboard", "--first-name", "Ole", "--last-name", "Christiansen"],
    ):
        main()


@then(u"perform all onboarding activities at once")
def step_impl(context):
    output = context.stdout_capture.getvalue()
    assert output == "Full onboard\n"


@given(u"we call klickbrick onboard --dev-tools --dry-run")
def step_impl(context):
    with patch.object(sys, "argv", ["klick", "--dry-run", "onboard", "--dev-tools"]):
        main()


@then(u"prints the operations that would not cause any system side effects to stdout")
def step_impl(context):
    output = context.stdout_capture.getvalue()
    assert "dry-run" in output
