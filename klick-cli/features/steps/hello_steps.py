from behave import *
import sys
from klick_cli.klickbrick import main
from mock import patch


def mainex(context):
    # Call DUT main but not exit() on exception because it
    # breaks the rest of tests.
    try:
        main()
    except:
        pass


@when("we call hello command")
def step_impl(context):
    # Use patch to pass argv to DUT
    with patch.object(sys, "argv", ["klick", "hello"]):
        main()


@then("we get 'Hello World' response")
def step_impl(context):
    output = context.stdout_capture.getvalue()
    assert output == "Hello World\n"


@When("we call hello with -n {parameter}")
def step_impl(context, parameter):
    with patch.object(sys, "argv", ["klick", "hello", "--name", parameter]):
        main()


@Then("we get 'Hello {parameter}'")
def step_impl(context, parameter):
    output = context.stdout_capture.getvalue()
    assert output == "Hello " + parameter + "\n"


@given(u"we call klick with unknown command")
def step_impl(context):
    with patch.object(sys, "argv", ["klick", "unknown_command"]):
        # with patch.object(sys, 'argv', ['klick', 'hello']):
        mainex(context)


@then(u"it should show invalid choice")
def step_impl(context):
    output = context.stderr_capture.getvalue()
    assert "invalid choice" in output
