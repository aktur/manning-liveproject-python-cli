Feature: onboard

  Scenario: test checklist
    Given we call onboard --checklist
    Then we get checklist

  Scenario: test template email
    Given we call onboard --it-request --first-name Ole --last-name Christiansen
    Then send email

  Scenario: test developer tools
    Given we call onboard --dev-tools
    Then install all tools

  Scenario: test developer specific tool
    Given we call onboard --dev-tools [git | pyenv | poetry]
    Then install specific tool

  Scenario: full onboard
    Given we call klickbrick onboard --first-name Ole --last-name Christiansen
    Then perform all onboarding activities at once

  Scenario: dry run
    Given we call klickbrick onboard --dev-tools --dry-run
    Then prints the operations that would not cause any system side effects to stdout